package com.olkhoviy.passive.controller;
import com.olkhoviy.passive.model.Device;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * Class menu contains methods for work with Devices
 */
public class Menu {
    /**
     * Method for invoke main menu
     */
    public static void invokeMenu() {
        System.out.println("Choose your option :");
        System.out.println("1 - for turn on device");
        System.out.println("2 - for turn off device");
        System.out.println("3 - for sum electric power of devices");
        System.out.println("4 - show sorted list of devices");
        System.out.println("0 - for exit");
    }

    /**
     * Method for invoke device menu
     */
    public static void chooseDevice() {
        System.out.println("0 - Refrigerator");
        System.out.println("1 - Vaccum cleaner");
        System.out.println("2 - Washing machine");
        System.out.println("3 - Head dryer");
        System.out.println("4 - Table lamp");
        System.out.println("5 - TV");
    }

    /**
     * The main method for work with devices
     * Contain array of devices
     * And cycles for work with user
     * @throws IOException
     * And catch NumberFormatException if user write incorrect field
     */
    public static void checkAnswer() {
        Device[] devices = new Device[6];
        devices[0] = new Device("Refrigerator", 540);
        devices[1] = new Device("Vacuum cleaner", 700);
        devices[2] = new Device("Washing machine", 2000);
        devices[3] = new Device("Head dryer", 1200);
        devices[4] = new Device("Table lamp", 16);
        devices[5] = new Device("TV", 150);

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

            int point = 0;
            do {
                invokeMenu();
                int answer = Integer.parseInt(reader.readLine().substring(0, 1));
                switch (answer) {
                    case 1:
                        point++;
                        System.out.println("Wich device do you want to turn on?");
                        chooseDevice();
                        int choise = Integer.parseInt(reader.readLine().substring(0, 1));
                        switch (choise) {
                            case 0:
                                devices[choise].turnOn();
                                System.out.println("Device successful switched on");
                                break;
                            case 1:
                                devices[choise].turnOn();
                                System.out.println("Device successful switched on");
                                break;
                            case 2:
                                devices[choise].turnOn();
                                System.out.println("Device successful switched on");
                                break;
                            case 3:
                                devices[choise].turnOn();
                                System.out.println("Device successful switched on");
                                break;
                            case 4:
                                devices[choise].turnOn();
                                System.out.println("Device successful switched on");
                                break;
                        }
                        break;
                    case 2:
                        point++;
                        System.out.println("Wich device dou you want to switch off?");
                        chooseDevice();
                        int choiseForOff = Integer.parseInt(reader.readLine().substring(0, 1));
                        switch (choiseForOff) {
                            case 0:
                                devices[choiseForOff].turnOff();
                                System.out.println("Device successful switched off");
                                break;
                            case 1:
                                devices[choiseForOff].turnOff();
                                System.out.println("Device successful switched off");
                                break;
                            case 2:
                                devices[choiseForOff].turnOff();
                                System.out.println("Device successful switched off");
                                break;
                            case 3:
                                devices[choiseForOff].turnOff();
                                System.out.println("Device successful switched off");
                                break;
                            case 4:
                                devices[choiseForOff].turnOff();
                                System.out.println("Device successful switched on");
                                break;
                        }
                        break;
                    case 3:
                        point++;
                        int electircPower=0;
                        for (int i =0;i < devices.length;i++){
                            electircPower = electircPower + devices[i].getEnergyCount();
                        }
                        System.out.println("Sum of energy is " + electircPower + " Kvt");
                        System.out.print('\n');
                        break;
                    case 4:
                        point++;
                        Arrays.sort(devices);
                        for (int i =0;i<devices.length;i++){
                            System.out.println(devices[i]);
                        }
                        System.out.print('\n');
                        break;
                    case 0:
                        point = 0;
                        System.exit(1);
                        break;
                }
            } while (point != 0);

        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (NumberFormatException num) {
            System.out.println("Please , enter number only");
            System.out.print('\n');
            checkAnswer();
        }


    }
}
