package com.olkhoviy.passive;

import com.olkhoviy.passive.controller.Menu;
/**
 * The application class.
 * <h1>Task03_oop </h1>
 *
 * @author Ivan Olkhoviy
 */

public class Application {
    public static void main(String[] args) {
        /**
         * The entry point of application.
         * Greetings and menu invocation
         * @param args the input arguments
         */
        System.out.println("Hello!");
        Menu.checkAnswer();
    }
}
