package com.olkhoviy.passive.model;
/**
 * Class Device is a class for all home devices
 * Implements Comparable for sorting
 */
public class Device implements Comparable {
    private String name;
    private int energyCount;
    private boolean power;

    /**
     * Constructor
     * @param name
     * @param energyCount
     */
    public Device(String name, int energyCount) {
        this.name = name;
        this.energyCount = energyCount;
    }

    /**
     *There are setters and getters
     */
    public String getName() {
        return name;
    }

    public int getEnergyCount() {
        return energyCount;
    }


    public boolean isPower() {
        return power;
    }

    /**
     * This method for turn On some device
     */
    public void turnOn() {
        this.power = power;
        power = true;
    }

    /**
     * This method for turn Off device
     */
    public void turnOff() {
        this.power = power;
        power = true;
    }

    @Override
    public String toString() {
        return name + " " + energyCount + " Vt";
    }

    /**
     * Impl for method compareTo for sorting by variable energyCount
     * @param obj
     */
    public int compareTo(Object obj){
        Device dev = (Device) obj;
        if (this.getEnergyCount() < dev.getEnergyCount()){
            return -1;
        }else if (this.getEnergyCount() > dev.getEnergyCount()){
            return 1;
        }return 0;
}

}
